package middleware

import (
	"context"
	"encoding/json"
	"net/http"
	"strings"

	firebase "firebase.google.com/go"
)

// CustomError struct
type CustomError struct {
	Message string
}

// AuthMiddleware middlware
func AuthMiddleware(firebase *firebase.App) func(http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			reqToken := r.Header.Get("Authorization")

			if reqToken == "" {
				next.ServeHTTP(w, r)
				return
			}

			withToken := strings.Split(reqToken, "Bearer ")
			accesstoken := withToken[1]
			ctx := context.Background()

			client, err := firebase.Auth(ctx)
			token, err := client.VerifyIDToken(ctx, accesstoken)

			if err != nil {
				data := CustomError{
					Message: "Unauthorized",
				}

				w.Header().Set("Content-Type", "application/json")
				w.WriteHeader(http.StatusUnauthorized)
				json.NewEncoder(w).Encode(data)
				return
			}

			// put it in context
			ctx = context.WithValue(r.Context(), "userID", token.UID)

			// and call the next with our new context
			r = r.WithContext(ctx)
			next.ServeHTTP(w, r)

		})
	}
}

// ForContext finds the user from the context. REQUIRES Middleware to have run.
func ForContext(ctx context.Context) string {
	raw, _ := ctx.Value("userID").(string)
	return raw
}
