package helper

import (
	"log"

	"google.golang.org/grpc"
)

// ConnectGRPC method
func ConnectGRPC(address string) *grpc.ClientConn {
	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())

	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}

	return conn
}
