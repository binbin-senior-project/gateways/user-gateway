package helper

import (
	"fmt"

	"golang.org/x/net/context"

	firebase "firebase.google.com/go"

	"google.golang.org/api/option"
)

// NewFirebaeAdmin function
func NewFirebaeAdmin() (*firebase.App, error) {

	creds := []byte(`
		{
			"type": "service_account",
			"project_id": "binbin-fbf72",
			"private_key_id": "f3f1952cceb8e3f6b4669bae591aa139786888b9",
			"private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCV2zqD8OYQE/Gc\n/dwXiZ5aypuhGZe5Fdr8nxl/MAwIWyD8W4lDc5INCPPC/oSoUi0tn2bVW65k6IiO\n6aw38KX7pQ5oE8S8UVOdmKCD/H6EYXS5cj1kKi/pPhGUs4Wn5i9L6RZflM2YV4Qr\nV1rEh10v7N1A0sMpDeGRh/ZOcvsRahQIXPEfC09zLdyZDUlMMti2GRpFcoA//cDr\nBAYdWQbk33DYtkF+u5tfXmJ+I5XmzpQBBY3QyRpax4UmCEoZZXHOD6hV+fGEmeIu\n3PPll8JrcriOlzLHmw/VnMpanfmNvkIeg+u1i8VJ0itePLZgSDrrCDTqbnbPmPym\nBae16XvjAgMBAAECggEAJ/txbdGAuGpxzOaoablEgEnNYxqucenkXwFd7qLVmwch\nUmNjyJTVs1efnfeFUll2h1UfvS40ZeTqxmKPGe/BgVYE9XzMwUfbjNEzhnOAF/be\niznxKj/dP5hZXXjWYLkYBmNenkrX3dz/vQaWB5hsIwTJ9WkmO9ZhyA/kBgNEfF0N\n6zwBBXohZR+lHdt0njVHBsRuo6IGhzY/H/82+08/gQbVJfthsf4szDlDU6Lixq0r\ncfN5TOepVM2tEQS8bpJjISMNf9HXuOGkLl9SMadVpiAy6fHNBOGuX4V9CFuCqbz5\nBlIkQBlKaCbEtN12RVXcJ+qy+M6xT9fxEW7qeLOdWQKBgQDOGeMvI1v/cTy+PMVE\niabyvkQyemkQvAEMP1maz+8CiE6rfAylfH0fIiTQpu5/wzGzDrgsjDbJ3HFW5Kq7\nqd0z1sVdbUDK9mj5myJJlDWWq9pN3TQA3PMV/nJBAfqmFnjRSu2n3BU7uAuygk0s\nJuczt7qcg6QfHuRd7K+HqCF8awKBgQC6I0zm90D3qXAwZdVUvV19vYLS3t9imVgJ\nbenfTOI6LUGoR+qNj2tAmCwKe0SZfpzmHOaMG7tbLoXSzKF17/DtbVfEc2M9BLAE\npoF+DVckWv54Hci6kf03uykX9q0qDvCK8tPqfmWzU1hCTHCRA1h4g/Bbjf7sQZnS\nThQxNCVcaQKBgFdkGLXrK15EGLqdsBPgvl6dmPNCfnxvC46y6f6//pu3SoxiPEaL\nY86ovq2OqwCXtuIy8ptJFQ5BH/n1WgPbAgWyItv7sIafN2HniwFiC9sIobd0fVGE\nXrrLuoygFzUHwFEWkXEk3AgHgUY/BBQdptmCJWmqVBQyGClPbBp8iz5jAoGAAnCS\nAnj9LgHXiZrcF/c/cXV5cIAhV26BG+aH6ThdwrtzH9yJc3+MvVUBRsXk2UoyjkWU\ntANtCj3q3R93gk85zjLfsqB8jYExOW5hRiCworZanYJubhuZwRA/8qN2E5YMixWx\nt2IevSH7hyrvUsdDcpjwHpMW40vc00fYtehkEPkCgYAQ8UXJHhIJKS418hIQFRVD\n1gq17jj8vWeGGCtV74PxvMGbHTcyS7PTGpem2XMNgTzW/YFIdXYOGgmD2J8dY4OW\nmjgITIa+qq1IoGjD9/eHDwhJ+JzxcwBuQDkcPKaoLYLzKpyILO8MORMDx1j7MEX1\nz+/VGM7XRg3PwMiy0+HhhA==\n-----END PRIVATE KEY-----\n",
			"client_email": "firebase-adminsdk-40nps@binbin-fbf72.iam.gserviceaccount.com",
			"client_id": "100162056898176640824",
			"auth_uri": "https://accounts.google.com/o/oauth2/auth",
			"token_uri": "https://oauth2.googleapis.com/token",
			"auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
			"client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-40nps%40binbin-fbf72.iam.gserviceaccount.com"
		}	
	`)

	opt := option.WithCredentialsJSON(creds)
	app, err := firebase.NewApp(context.Background(), nil, opt)

	if err != nil {
		return nil, fmt.Errorf("error initializing app: %v", err)
	}

	return app, nil

}
