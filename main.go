package main

import (
	"log"
	"net/http"
	"os"
	"time"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/go-chi/chi"
	"github.com/joho/godotenv"
	"gitlab.com/mangbinbin/gateways/user-gateway/graph"
	"gitlab.com/mangbinbin/gateways/user-gateway/graph/generated"
	h "gitlab.com/mangbinbin/gateways/user-gateway/helper"
	mw "gitlab.com/mangbinbin/gateways/user-gateway/middleware"
)

func main() {

	godotenv.Load()
	port := os.Getenv("SERVICE_PORT")
	http.DefaultClient.Timeout = time.Minute * 10

	firebase, err := h.NewFirebaeAdmin()

	if err != nil {
		log.Println(err.Error())
	}

	router := chi.NewRouter()
	router.Use(mw.AuthMiddleware(firebase))

	srv := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{Resolvers: &graph.Resolver{}}))

	router.Handle("/", playground.Handler("GraphQL playground", "/query"))
	router.Handle("/query", srv)

	log.Printf("connect to http://localhost:%s/ for GraphQL playground", port)
	log.Fatal(http.ListenAndServe(":"+port, router))
}
