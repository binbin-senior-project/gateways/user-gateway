module gitlab.com/mangbinbin/gateways/user-gateway

go 1.13

require (
	firebase.google.com/go v3.12.1+incompatible
	github.com/99designs/gqlgen v0.11.3
	github.com/go-chi/chi v4.1.1+incompatible
	github.com/golang/protobuf v1.4.0
	github.com/joho/godotenv v1.3.0
	github.com/vektah/gqlparser/v2 v2.0.1
	golang.org/x/net v0.0.0-20200425230154-ff2c4b7c35a0
	google.golang.org/api v0.22.0
	google.golang.org/grpc v1.29.1
)
