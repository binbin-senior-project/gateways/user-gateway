package graph

// This file will not be regenerated automatically.
//
// It serves as dependency injection for your app, add any dependencies you require here.

import (
	"os"

	h "gitlab.com/mangbinbin/gateways/user-gateway/helper"
	pb "gitlab.com/mangbinbin/gateways/user-gateway/proxy"
)

// Resolver struct
type Resolver struct{}

// NewUserService method
func (r *Resolver) NewUserService() pb.UserServiceClient {
	addr := os.Getenv("USER_SERVICE_ADDRESS")
	conn := h.ConnectGRPC(addr)
	return pb.NewUserServiceClient(conn)
}

// NewStoreService method
func (r *Resolver) NewStoreService() pb.StoreServiceClient {
	addr := os.Getenv("STORE_SERVICE_ADDRESS")
	conn := h.ConnectGRPC(addr)
	return pb.NewStoreServiceClient(conn)
}

// NewPointService method
func (r *Resolver) NewPointService() pb.PointServiceClient {
	addr := os.Getenv("POINT_SERVICE_ADDRESS")
	conn := h.ConnectGRPC(addr)
	return pb.NewPointServiceClient(conn)
}

// NewRedeemService method
func (r *Resolver) NewRedeemService() pb.RedeemServiceClient {
	addr := os.Getenv("REDEEM_SERVICE_ADDRESS")
	conn := h.ConnectGRPC(addr)
	return pb.NewRedeemServiceClient(conn)
}

// NewNotificationService method
func (r *Resolver) NewNotificationService() pb.NotificationServiceClient {
	addr := os.Getenv("NOTIFICATION_SERVICE_ADDRESS")
	conn := h.ConnectGRPC(addr)
	return pb.NewNotificationServiceClient(conn)
}

// NewSMSService method
func (r *Resolver) NewSMSService() pb.SMSServiceClient {
	addr := os.Getenv("SMS_SERVICE_ADDRESS")
	conn := h.ConnectGRPC(addr)
	return pb.NewSMSServiceClient(conn)
}
