// Code generated by github.com/99designs/gqlgen, DO NOT EDIT.

package model

type Bin struct {
	ID          string  `json:"id"`
	Name        string  `json:"name"`
	Description string  `json:"description"`
	Status      string  `json:"status"`
	Peripheral  string  `json:"peripheral"`
	Latitude    float64 `json:"latitude"`
	Longitude   float64 `json:"longitude"`
}

type CollectPointInput struct {
	TrashCodes []string `json:"trashCodes"`
}

type CouponCategory struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type FavoriteCouponInput struct {
	CouponID string `json:"couponID"`
}

type IsEmailExistInput struct {
	Email string `json:"email"`
}

type IsPhoneExistInput struct {
	Phone string `json:"phone"`
}

type NewUser struct {
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
	Email     string `json:"email"`
	Phone     string `json:"phone"`
	Photo     string `json:"photo"`
	Passcode  string `json:"passcode"`
}

type RedeemCouponInput struct {
	CouponID string `json:"couponID"`
}

type RequestOTPInput struct {
	Phone string `json:"phone"`
}

type Status struct {
	Success bool `json:"success"`
}

type StoreCategory struct {
	ID   string `json:"id"`
	Name string `json:"name"`
}

type TokenMessagingInput struct {
	Token string `json:"token"`
}

type Transaction struct {
	ID          string  `json:"id"`
	Type        string  `json:"type"`
	Point       float64 `json:"point"`
	Description string  `json:"description"`
	CreatedAt   string  `json:"createdAt"`
}

type TransferPointInput struct {
	Phone string  `json:"phone"`
	Point float64 `json:"point"`
}

type Trash struct {
	ID           string  `json:"id"`
	Name         string  `json:"name"`
	Code         string  `json:"code"`
	RegularPoint float64 `json:"regularPoint"`
	SalePoint    float64 `json:"salePoint"`
}

type UnFavoriteCouponInput struct {
	CouponID string `json:"couponID"`
}

type UpdateUserInput struct {
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
	Photo     string `json:"photo"`
}

type User struct {
	Firstname string  `json:"firstname"`
	Lastname  string  `json:"lastname"`
	Email     string  `json:"email"`
	Phone     string  `json:"phone"`
	Photo     string  `json:"photo"`
	Point     float64 `json:"point"`
	Status    string  `json:"status"`
}

type VerifyOTPInput struct {
	Phone string `json:"phone"`
	Code  string `json:"code"`
}

type VerifyPasscodeInput struct {
	Passcode string `json:"passcode"`
}

type VerifyTrashInput struct {
	TrashCode string `json:"trashCode"`
}
