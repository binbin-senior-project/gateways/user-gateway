package model

type Coupon struct {
	ID          string  `json:"id"`
	Photo       string  `json:"photo"`
	Name        string  `json:"name"`
	Description string  `json:"description"`
	Point       float64 `json:"point"`
	Condition   string  `json:"condition"`
	Expire      string  `json:"expire"`
	Status      string  `json:"status"`
	StoreID     string  `json:"store"`
	CategoryID  string  `json:"category"`
}

type Store struct {
	ID         string  `json:"id"`
	Logo       string  `json:"logo"`
	Name       string  `json:"name"`
	Tagline    string  `json:"tagline"`
	Phone      string  `json:"phone"`
	Latitude   float64 `json:"latitude"`
	Longitude  float64 `json:"longitude"`
	CategoryID string  `json:"category"`
}
