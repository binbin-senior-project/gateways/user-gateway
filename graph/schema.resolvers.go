package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"

	"gitlab.com/mangbinbin/gateways/user-gateway/graph/generated"
	"gitlab.com/mangbinbin/gateways/user-gateway/graph/model"
	mw "gitlab.com/mangbinbin/gateways/user-gateway/middleware"
	pb "gitlab.com/mangbinbin/gateways/user-gateway/proxy"
)

func (r *couponResolver) Store(ctx context.Context, obj *model.Coupon) (*model.Store, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return &model.Store{}, fmt.Errorf("Access denied")
	}

	s := r.NewStoreService()

	rsp, err := s.GetStore(ctx, &pb.GetStoreRequest{
		StoreID: obj.StoreID,
	})

	if err != nil {
		return &model.Store{}, err
	}

	return &model.Store{
		ID:         rsp.Store.Id,
		Logo:       rsp.Store.Logo,
		Name:       rsp.Store.Name,
		Tagline:    rsp.Store.Tagline,
		Phone:      rsp.Store.Phone,
		Latitude:   rsp.Store.Latitude,
		Longitude:  rsp.Store.Longitude,
		CategoryID: rsp.Store.CategoryID,
	}, nil
}

func (r *couponResolver) Category(ctx context.Context, obj *model.Coupon) (*model.CouponCategory, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return &model.CouponCategory{}, fmt.Errorf("Access denied")
	}

	s := r.NewRedeemService()

	rsp, err := s.GetCouponCategory(ctx, &pb.GetCouponCategoryRequest{
		CategoryID: obj.CategoryID,
	})

	if err != nil {
		return &model.CouponCategory{}, err
	}

	return &model.CouponCategory{
		ID:   rsp.Category.Id,
		Name: rsp.Category.Name,
	}, nil
}

func (r *couponResolver) IsFavorite(ctx context.Context, obj *model.Coupon) (bool, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return false, fmt.Errorf("Access denied")
	}

	s := r.NewRedeemService()

	rsp, err := s.CheckFavoriteCoupon(ctx, &pb.CheckFavoriteCouponRequest{
		UserID:   userID,
		CouponID: obj.ID,
	})

	if err != nil {
		return false, err
	}

	return rsp.Success, nil
}

func (r *mutationResolver) SetTokenMessaging(ctx context.Context, input model.TokenMessagingInput) (*model.Status, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return &model.Status{Success: false}, fmt.Errorf("Access denied")
	}

	s := r.NewNotificationService()

	_, err := s.SetToken(ctx, &pb.SetTokenRequest{
		UserID: userID,
		Token:  input.Token,
		Type:   "user",
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) CreateUser(ctx context.Context, input model.NewUser) (*model.Status, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return &model.Status{Success: false}, fmt.Errorf("Access denied")
	}

	s := r.NewUserService()

	_, err := s.CreateUser(ctx, &pb.CreateUserRequest{
		Id:        userID,
		Firstname: input.Firstname,
		Lastname:  input.Lastname,
		Email:     input.Email,
		Phone:     input.Phone,
		Photo:     input.Photo,
		Passcode:  input.Passcode,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) IsEmailExist(ctx context.Context, input model.IsEmailExistInput) (*model.Status, error) {
	s := r.NewUserService()

	rsp, err := s.IsEmailExist(ctx, &pb.IsEmailExistRequest{
		Email: input.Email,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: rsp.Success}, nil
}

func (r *mutationResolver) IsPhoneExist(ctx context.Context, input model.IsPhoneExistInput) (*model.Status, error) {
	s := r.NewUserService()

	rsp, err := s.IsPhoneExist(ctx, &pb.IsPhoneExistRequest{
		Phone: input.Phone,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: rsp.Success}, nil
}

func (r *mutationResolver) VerifyPasscode(ctx context.Context, input model.VerifyPasscodeInput) (*model.Status, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return &model.Status{Success: false}, fmt.Errorf("Access denied")
	}

	s := r.NewUserService()

	rsp, err := s.VerifyPasscode(ctx, &pb.VerifyPasscodeRequest{
		UserID:   userID,
		Passcode: input.Passcode,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: rsp.Success}, nil
}

func (r *mutationResolver) RequestOtp(ctx context.Context, input model.RequestOTPInput) (*model.Status, error) {
	s := r.NewSMSService()

	_, err := s.RequestOTP(ctx, &pb.RequestOTPRequest{
		Phone: input.Phone,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) VerifyOtp(ctx context.Context, input model.VerifyOTPInput) (*model.Status, error) {
	s := r.NewSMSService()

	rsp, err := s.VerifyOTP(ctx, &pb.VerifyOTPRequest{
		Phone: input.Phone,
		Code:  input.Code,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: rsp.Success}, nil
}

func (r *mutationResolver) UpdateUser(ctx context.Context, input model.UpdateUserInput) (*model.Status, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return &model.Status{Success: false}, fmt.Errorf("Access denied")
	}

	s := r.NewUserService()

	_, err := s.UpdateUser(ctx, &pb.UpdateUserRequest{
		UserID:    userID,
		Firstname: input.Firstname,
		Lastname:  input.Lastname,
		Photo:     input.Photo,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) CollectPoint(ctx context.Context, input model.CollectPointInput) (*model.Status, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return &model.Status{Success: false}, fmt.Errorf("Access denied")
	}

	s := r.NewPointService()

	_, err := s.CollectPoint(ctx, &pb.CollectPointRequest{
		UserID:     userID,
		TrashCodes: input.TrashCodes,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) TransferPoint(ctx context.Context, input model.TransferPointInput) (*model.Status, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return &model.Status{Success: false}, fmt.Errorf("Access denied")
	}

	s := r.NewPointService()

	_, err := s.TransferPoint(ctx, &pb.TransferPointRequest{
		UserID: userID,
		Phone:  input.Phone,
		Point:  input.Point,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) RedeemCoupon(ctx context.Context, input model.RedeemCouponInput) (*model.Status, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return &model.Status{Success: false}, fmt.Errorf("Access denied")
	}

	s := r.NewRedeemService()

	_, err := s.RedeemCoupon(ctx, &pb.RedeemCouponRequest{
		UserID:   userID,
		CouponID: input.CouponID,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) FavoriteCoupon(ctx context.Context, input model.FavoriteCouponInput) (*model.Status, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return &model.Status{Success: false}, fmt.Errorf("Access denied")
	}

	s := r.NewRedeemService()

	_, err := s.FavoriteCoupon(ctx, &pb.FavoriteCouponRequest{
		UserID:   userID,
		CouponID: input.CouponID,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *mutationResolver) UnFavoriteCoupon(ctx context.Context, input model.UnFavoriteCouponInput) (*model.Status, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return &model.Status{Success: false}, fmt.Errorf("Access denied")
	}

	s := r.NewRedeemService()

	_, err := s.UnFavoriteCoupon(ctx, &pb.UnFavoriteCouponRequest{
		UserID:   userID,
		CouponID: input.CouponID,
	})

	if err != nil {
		return &model.Status{Success: false}, err
	}

	return &model.Status{Success: true}, nil
}

func (r *queryResolver) GetUser(ctx context.Context) (*model.User, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return &model.User{}, fmt.Errorf("Access denied")
	}

	s := r.NewUserService()

	rsp, err := s.GetUser(ctx, &pb.GetUserRequest{
		UserID: userID,
	})

	if err != nil {
		return &model.User{}, err
	}

	return &model.User{
		Firstname: rsp.User.Firstname,
		Lastname:  rsp.User.Lastname,
		Email:     rsp.User.Email,
		Phone:     rsp.User.Phone,
		Photo:     rsp.User.Photo,
		Point:     rsp.User.Point,
		Status:    rsp.User.Status,
	}, nil
}

func (r *queryResolver) GetUserByPhone(ctx context.Context, phone string) (*model.User, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return &model.User{}, fmt.Errorf("Access denied")
	}

	s := r.NewUserService()

	rsp, err := s.GetUserByPhone(ctx, &pb.GetUserByPhoneRequest{
		Phone: phone,
	})

	if err != nil {
		return &model.User{}, nil
	}

	return &model.User{
		Firstname: rsp.User.Firstname,
		Lastname:  rsp.User.Lastname,
		Email:     rsp.User.Email,
		Phone:     rsp.User.Phone,
		Photo:     rsp.User.Photo,
		Point:     rsp.User.Point,
		Status:    rsp.User.Status,
	}, nil
}

func (r *queryResolver) GetCoupons(ctx context.Context, offset int, limit int) ([]*model.Coupon, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return []*model.Coupon{}, fmt.Errorf("Access denied")
	}

	s := r.NewRedeemService()

	var coupons []*model.Coupon

	rsp, err := s.GetApproveCoupons(ctx, &pb.GetApproveCouponsRequest{
		Offset: int32(offset),
		Limit:  int32(limit),
	})

	if err != nil {
		return coupons, err
	}

	for _, coupon := range rsp.Coupons {

		coupons = append(coupons, &model.Coupon{
			ID:          coupon.Id,
			Name:        coupon.Name,
			Description: coupon.Description,
			Point:       coupon.Point,
			Condition:   coupon.Condition,
			Expire:      coupon.Expire,
			Status:      coupon.Status,
			Photo:       coupon.Photo,
			StoreID:     coupon.StoreID,
			CategoryID:  coupon.CategoryID,
		})
	}

	return coupons, nil
}

func (r *queryResolver) GetCouponsByCategory(ctx context.Context, categoryID string, offset int, limit int) ([]*model.Coupon, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return []*model.Coupon{}, fmt.Errorf("Access denied")
	}

	s := r.NewRedeemService()

	var coupons []*model.Coupon

	rsp, err := s.GetCouponsByCategory(ctx, &pb.GetCouponsByCategoryRequest{
		CategoryID: categoryID,
		Offset:     int32(offset),
		Limit:      int32(limit),
	})

	if err != nil {
		return coupons, err
	}

	for _, coupon := range rsp.Coupons {
		coupons = append(coupons, &model.Coupon{
			ID:          coupon.Id,
			Name:        coupon.Name,
			Description: coupon.Description,
			Point:       coupon.Point,
			Condition:   coupon.Condition,
			Expire:      coupon.Expire,
			Status:      coupon.Status,
			Photo:       coupon.Photo,
			StoreID:     coupon.StoreID,
			CategoryID:  coupon.CategoryID,
		})
	}

	return coupons, nil
}

func (r *queryResolver) GetCouponsBySearch(ctx context.Context, keyword string, offset int, limit int) ([]*model.Coupon, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return []*model.Coupon{}, fmt.Errorf("Access denied")
	}

	s := r.NewRedeemService()

	var coupons []*model.Coupon

	rsp, err := s.GetCouponsByKeyword(ctx, &pb.GetCouponsByKeywordRequest{
		Keyword: keyword,
		Offset:  int32(offset),
		Limit:   int32(limit),
	})

	if err != nil {
		return coupons, err
	}

	for _, coupon := range rsp.Coupons {
		coupons = append(coupons, &model.Coupon{
			ID:          coupon.Id,
			Name:        coupon.Name,
			Description: coupon.Description,
			Point:       coupon.Point,
			Condition:   coupon.Condition,
			Expire:      coupon.Expire,
			Status:      coupon.Status,
			Photo:       coupon.Photo,
			StoreID:     coupon.StoreID,
			CategoryID:  coupon.CategoryID,
		})
	}

	return coupons, nil
}

func (r *queryResolver) GetCoupon(ctx context.Context, couponID string) (*model.Coupon, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return &model.Coupon{}, fmt.Errorf("Access denied")
	}

	s := r.NewRedeemService()

	rsp, err := s.GetCoupon(ctx, &pb.GetCouponRequest{
		CouponID: couponID,
	})

	if err != nil {
		return &model.Coupon{}, err
	}

	return &model.Coupon{
		ID:          rsp.Coupon.Id,
		Name:        rsp.Coupon.Name,
		Description: rsp.Coupon.Description,
		Point:       rsp.Coupon.Point,
		Condition:   rsp.Coupon.Condition,
		Expire:      rsp.Coupon.Expire,
		Status:      rsp.Coupon.Status,
		Photo:       rsp.Coupon.Photo,
		StoreID:     rsp.Coupon.StoreID,
		CategoryID:  rsp.Coupon.CategoryID,
	}, nil
}

func (r *queryResolver) GetCouponCategories(ctx context.Context, offset int, limit int) ([]*model.CouponCategory, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return []*model.CouponCategory{}, fmt.Errorf("Access denied")
	}

	s := r.NewRedeemService()

	var categories []*model.CouponCategory

	rsp, err := s.GetCouponCategories(ctx, &pb.GetCouponCategoriesRequest{
		Offset: int32(offset),
		Limit:  int32(limit),
	})

	if err != nil {
		return categories, err
	}

	for _, category := range rsp.Categories {
		categories = append(categories, &model.CouponCategory{
			ID:   category.Id,
			Name: category.Name,
		})
	}

	return categories, nil
}

func (r *queryResolver) GetFavoriteCoupons(ctx context.Context, offset int, limit int) ([]*model.Coupon, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return []*model.Coupon{}, fmt.Errorf("Access denied")
	}

	s := r.NewRedeemService()

	var coupons []*model.Coupon

	rsp, err := s.GetFavoriteCoupons(ctx, &pb.GetFavoriteCouponsRequest{
		UserID: userID,
		Offset: int32(offset),
		Limit:  int32(limit),
	})

	if err != nil {
		return coupons, err
	}

	for _, coupon := range rsp.Coupons {

		coupons = append(coupons, &model.Coupon{
			ID:          coupon.Id,
			Name:        coupon.Name,
			Description: coupon.Description,
			Point:       coupon.Point,
			Condition:   coupon.Condition,
			Expire:      coupon.Expire,
			Status:      coupon.Status,
			Photo:       coupon.Photo,
			StoreID:     coupon.StoreID,
			CategoryID:  coupon.CategoryID,
		})
	}

	return coupons, nil
}

func (r *queryResolver) GetStores(ctx context.Context, offset int, limit int) ([]*model.Store, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return []*model.Store{}, fmt.Errorf("Access denied")
	}

	s := r.NewStoreService()

	var stores []*model.Store

	rsp, err := s.GetStores(ctx, &pb.GetStoresRequest{
		Offset: int32(offset),
		Limit:  int32(limit),
	})

	if err != nil {
		return stores, err
	}

	for _, store := range rsp.Stores {
		stores = append(stores, &model.Store{
			ID:         store.Id,
			Logo:       store.Logo,
			Name:       store.Name,
			Tagline:    store.Tagline,
			Phone:      store.Phone,
			Latitude:   store.Latitude,
			Longitude:  store.Longitude,
			CategoryID: store.CategoryID,
		})
	}

	return stores, nil
}

func (r *queryResolver) GetStore(ctx context.Context, storeID string) (*model.Store, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return &model.Store{}, fmt.Errorf("Access denied")
	}

	s := r.NewStoreService()

	rsp, err := s.GetStore(ctx, &pb.GetStoreRequest{
		StoreID: storeID,
	})

	if err != nil {
		return &model.Store{}, err
	}

	return &model.Store{
		ID:         rsp.Store.Id,
		Logo:       rsp.Store.Logo,
		Name:       rsp.Store.Name,
		Tagline:    rsp.Store.Tagline,
		Phone:      rsp.Store.Phone,
		Latitude:   rsp.Store.Latitude,
		Longitude:  rsp.Store.Longitude,
		CategoryID: rsp.Store.CategoryID,
	}, nil
}

func (r *queryResolver) GetStoresByCategory(ctx context.Context, categoryID string, offset int, limit int) ([]*model.Store, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return []*model.Store{}, fmt.Errorf("Access denied")
	}

	s := r.NewStoreService()

	var stores []*model.Store

	rsp, err := s.GetStoresByCategory(ctx, &pb.GetStoresByCategoryRequest{
		CategoryID: categoryID,
		Offset:     int32(offset),
		Limit:      int32(limit),
	})

	if err != nil {
		return stores, err
	}

	for _, store := range rsp.Stores {
		stores = append(stores, &model.Store{
			ID:         store.Id,
			Logo:       store.Logo,
			Name:       store.Name,
			Tagline:    store.Tagline,
			Phone:      store.Phone,
			Latitude:   store.Latitude,
			Longitude:  store.Longitude,
			CategoryID: store.CategoryID,
		})
	}

	return stores, nil
}

func (r *queryResolver) GetStoresBySearch(ctx context.Context, keyword string, offset int, limit int) ([]*model.Store, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return []*model.Store{}, fmt.Errorf("Access denied")
	}

	s := r.NewStoreService()

	var rspStores []*model.Store

	rsp, err := s.GetStoresByKeyword(ctx, &pb.GetStoresByKeywordRequest{
		Keyword: keyword,
		Offset:  int32(offset),
		Limit:   int32(limit),
	})

	if err != nil {
		return rspStores, err
	}

	for _, store := range rsp.Stores {
		rspStores = append(rspStores, &model.Store{
			ID:         store.Id,
			Logo:       store.Logo,
			Name:       store.Name,
			Tagline:    store.Tagline,
			Phone:      store.Phone,
			Latitude:   store.Latitude,
			Longitude:  store.Longitude,
			CategoryID: store.CategoryID,
		})
	}

	return rspStores, nil
}

func (r *queryResolver) GetStoreCategories(ctx context.Context, offset int, limit int) ([]*model.StoreCategory, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return []*model.StoreCategory{}, fmt.Errorf("Access denied")
	}

	s := r.NewStoreService()

	var categories []*model.StoreCategory

	rsp, err := s.GetStoreCategories(ctx, &pb.GetStoreCategoriesRequest{
		Offset: int32(offset),
		Limit:  int32(limit),
	})

	if err != nil {
		return categories, err
	}

	for _, category := range rsp.Categories {
		categories = append(categories, &model.StoreCategory{
			ID:   category.Id,
			Name: category.Name,
		})
	}

	return categories, nil
}

func (r *queryResolver) GetTrashes(ctx context.Context, offset int, limit int) ([]*model.Trash, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return []*model.Trash{}, fmt.Errorf("Access denied")
	}

	s := r.NewPointService()

	var trashes []*model.Trash

	rsp, err := s.GetTrashes(ctx, &pb.GetTrashesRequest{
		Offset: int32(offset),
		Limit:  int32(limit),
	})

	if err != nil {
		return trashes, err
	}

	for _, trash := range rsp.Trashes {

		trashes = append(trashes, &model.Trash{
			ID:           trash.Id,
			Name:         trash.Name,
			Code:         trash.Code,
			RegularPoint: trash.RegularPoint,
			SalePoint:    trash.SalePoint,
		})
	}

	return trashes, nil
}

func (r *queryResolver) GetTrashByCode(ctx context.Context, code string) (*model.Trash, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return &model.Trash{}, fmt.Errorf("Access denied")
	}

	s := r.NewPointService()

	rsp, err := s.GetTrashByCode(ctx, &pb.GetTrashByCodeRequest{
		Code: code,
	})

	if err != nil {
		return &model.Trash{}, err
	}

	return &model.Trash{
		ID:           rsp.Trash.Id,
		Name:         rsp.Trash.Name,
		Code:         rsp.Trash.Code,
		RegularPoint: rsp.Trash.RegularPoint,
		SalePoint:    rsp.Trash.SalePoint,
	}, nil
}

func (r *queryResolver) GetBins(ctx context.Context, offset int, limit int) ([]*model.Bin, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return []*model.Bin{}, fmt.Errorf("Access denied")
	}

	s := r.NewPointService()

	var bins []*model.Bin

	rsp, err := s.GetBins(ctx, &pb.GetBinsRequest{
		Offset: int32(offset),
		Limit:  int32(limit),
	})

	if err != nil {
		return bins, err
	}

	for _, bin := range rsp.Bins {

		bins = append(bins, &model.Bin{
			ID:          bin.Id,
			Name:        bin.Name,
			Description: bin.Description,
			Status:      bin.Status,
			Peripheral:  bin.Peripheral,
			Latitude:    bin.Latitude,
			Longitude:   bin.Longitude,
		})
	}

	return bins, nil
}

func (r *queryResolver) GetTransactions(ctx context.Context, offset int, limit int) ([]*model.Transaction, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return []*model.Transaction{}, fmt.Errorf("Access denied")
	}

	s := r.NewUserService()

	var transactions []*model.Transaction

	rsp, err := s.GetUserTransactions(ctx, &pb.GetUserTransactionsRequest{
		UserID: userID,
		Offset: int32(offset),
		Limit:  int32(limit),
	})

	if err != nil {
		return transactions, err
	}

	for _, transaction := range rsp.Transactions {
		transactions = append(transactions, &model.Transaction{
			ID:          transaction.Id,
			Type:        transaction.Type,
			Point:       transaction.Point,
			Description: transaction.Description,
			CreatedAt:   transaction.CreatedAt,
		})
	}

	return transactions, nil
}

func (r *queryResolver) GetTransaction(ctx context.Context, transactionID string) (*model.Transaction, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return &model.Transaction{}, fmt.Errorf("Access denied")
	}

	s := r.NewUserService()

	rsp, err := s.GetUserTransaction(ctx, &pb.GetUserTransactionRequest{
		TransactionID: transactionID,
	})

	if err != nil {
		return &model.Transaction{}, err
	}

	return &model.Transaction{
		ID:          rsp.Transaction.Id,
		Type:        rsp.Transaction.Type,
		Point:       rsp.Transaction.Point,
		Description: rsp.Transaction.Description,
		CreatedAt:   rsp.Transaction.CreatedAt,
	}, nil
}

func (r *storeResolver) Category(ctx context.Context, obj *model.Store) (*model.StoreCategory, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return &model.StoreCategory{}, fmt.Errorf("Access denied")
	}

	s := r.NewStoreService()

	rsp, err := s.GetStoreCategory(ctx, &pb.GetStoreCategoryRequest{
		CategoryID: obj.CategoryID,
	})

	if err != nil {
		return &model.StoreCategory{}, err
	}

	return &model.StoreCategory{
		ID:   rsp.Category.Id,
		Name: rsp.Category.Name,
	}, nil
}

func (r *storeResolver) Coupons(ctx context.Context, obj *model.Store, offset int, limit int) ([]*model.Coupon, error) {
	userID := mw.ForContext(ctx)

	if userID == "" {
		return []*model.Coupon{}, fmt.Errorf("Access denied")
	}

	s := r.NewRedeemService()

	var coupons []*model.Coupon

	rsp, err := s.GetApproveCouponByStore(ctx, &pb.GetApproveCouponByStoreRequest{
		StoreID: obj.ID,
		Offset:  int32(offset),
		Limit:   int32(limit),
	})

	if err != nil {
		return coupons, err
	}

	for _, coupon := range rsp.Coupons {

		coupons = append(coupons, &model.Coupon{
			ID:          coupon.Id,
			Name:        coupon.Name,
			Description: coupon.Description,
			Point:       coupon.Point,
			Condition:   coupon.Condition,
			Expire:      coupon.Expire,
			Status:      coupon.Status,
			Photo:       coupon.Photo,
			StoreID:     coupon.StoreID,
			CategoryID:  coupon.CategoryID,
		})
	}

	return coupons, nil
}

// Coupon returns generated.CouponResolver implementation.
func (r *Resolver) Coupon() generated.CouponResolver { return &couponResolver{r} }

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

// Store returns generated.StoreResolver implementation.
func (r *Resolver) Store() generated.StoreResolver { return &storeResolver{r} }

type couponResolver struct{ *Resolver }
type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
type storeResolver struct{ *Resolver }
